class Version < Granite::Base
  adapter pg
  table_name versions

  belongs_to :hack

  primary id : Int64
  field name : String
  field comment : String
  field contributors : String
  field everdrive : String
  field release_date : String
  field download_url : String
  field is_deleted : Bool
  timestamps
end
