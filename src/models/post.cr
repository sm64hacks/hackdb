class Post < Granite::Base
  adapter pg
  table_name posts

  belongs_to :user

  belongs_to :topic

  primary id : Int64
  field title : String
  field content : String
  field ip_address : String
  timestamps
end
