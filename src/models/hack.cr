class Hack < Granite::Base
  adapter pg
  table_name hacks

  belongs_to :user
  has_many :version

  primary id : Int64
  field name : String
  field creator : String
  field description : String
  field download_url : String
  field difficulty : String
  field total_stars : String
  field required_stars : String
  field youtube_url : String
  field fandom_url : String
  field is_deleted : Bool
  timestamps

  def versions
    return Version.all("WHERE hack_id=#{@id} AND is_deleted=false").to_a
  end
end
