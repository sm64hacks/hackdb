class Flag < Granite::Base
  adapter pg
  table_name flags

  belongs_to :version

  primary id : Int64
  field reason : String
  field ipaddress : String
  field is_deleted : Bool
  timestamps
end
