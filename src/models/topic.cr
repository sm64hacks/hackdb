class Topic < Granite::Base
  adapter pg
  table_name topics

  belongs_to :user

  belongs_to :forum

  has_many :post

  primary id : Int64
  field title : String
  field summery : String
  timestamps
end
