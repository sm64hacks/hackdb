class Forum < Granite::Base
  adapter pg
  table_name forums

  primary id : Int64
  field title : String
  field description : String
  field topic_count : Int32
  field post_count : Int32
  timestamps
end
