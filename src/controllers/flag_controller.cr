class FlagController < ApplicationController
  def new
    hack = Hack.first("WHERE id = ?", [params[:id]])
    version = Version.first("WHERE id = ?", [params[:vid]])
    unless !hack
      unless !version
        render("new.ecr")
      end
    end
  end

  def create
    hack = Hack.first("WHERE id = ?", [params[:id]])
    version = Version.first("WHERE id = ?", [params[:vid]])
    unless !hack
      unless !version
        if request.headers.includes?("X-Forwarded-For")
          ip = request.headers["X-Forwarded-For"]
        else
          ip = "The request did not include an X-Forwarded-For header. Please check the server configuration for more details!"
        end
        Flag.create(reason: params[:reason], ipaddress: ip, version_id: version.id, is_deleted: false)
        flash[:info] = "Flag submitted for admin review. If you have further questions or concerns please email admin@sm64hacks.com"
        redirect_to "/hack/#{hack.id}"
      end
    end
  end
end
