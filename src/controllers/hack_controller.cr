class HackController < ApplicationController
  def index
    hacks = Hack.all("WHERE is_deleted = false AND NOT description = 'There is nothing for this ID in the old database' ORDER BY name ASC")
    render("index.ecr")
  end

  def creator
    hacks = Hack.all("WHERE is_deleted=false AND creator=?", [params[:creator]])
    render("creator.ecr")
  end

  def show
    hack_id = params[:id].to_i? || 0
    hack = Hack.first("WHERE is_deleted=false AND id=?", [hack_id])
    versions = hack.versions unless !hack
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !hack
      render("show.ecr")
    else
      flash[:danger] = "No such hack!"
      redirect_to "/hacks"
    end
  end

  def new
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      render("new.ecr")
    end
  end

  def create
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.create(name: params[:name], creator: params[:creator], description: params[:description], download_url: params[:download_url], difficulty: params[:difficulty], total_stars: params[:total_stars], required_stars: params[:required_stars], youtube_url: params[:youtube_url], fandom_url: params[:fandom_url], user_id: session[:user_id], is_deleted: false)
      flash[:info] = "Hack added to the database!"
      redirect_to "/hack/#{hack.id}"
    else
      flash[:danger] = "You must be an admin to add hacks to the database!"
      redirect_to "/hacks"
    end
  end

  def edit
    user = User.first("WHERE id = ?", [session[:user_id]])
    hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
    unless !user
      unless !hack
        render("edit.ecr")
      end
    end
  end

  def update
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      unless !hack
        hack.name = params[:name]
        hack.creator = params[:creator]
        hack.description = params[:description]
        hack.download_url = params[:download_url]
        hack.youtube_url = params[:youtube_url]
        hack.fandom_url = params[:fandom_url]
        hack.save
        flash[:info] = "Hack updated!"
        redirect_to "/hack/#{params[:id]}"
      else
        flash[:danger] = "The hack you attempted to update does not exist!"
        redirect_to "/hacks"
      end
    end
  end

  def delete
    user = User.first("WHERE id = ?", [session[:user_id]])
    hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
    unless !user
      unless !hack
        render("delete.ecr")
      end
    end
  end

  def confirmdelete
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      unless !hack
        hack.is_deleted = true
        hack.save
        flash[:info] = "Hack deleted!"
        redirect_to "/hacks"
      else
        flash[:danger] = "The hack you attempted to delete does not exist!"
        redirect_to "/hacks"
      end
    end
  end
end
