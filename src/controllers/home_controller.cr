class HomeController < ApplicationController
  def index
    render("index.ecr")
  end

  def api_doc
    render("api_doc.ecr")
  end
end
