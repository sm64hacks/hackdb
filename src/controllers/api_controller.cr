class ApiController < ApplicationController
  def get_hack
    hack = Hack.first("WHERE id=? AND is_deleted=false", [params[:id]])
    unless !hack
      versions = Version.all("WHERE hack_id=? AND is_deleted=false", [hack.id!]).to_a
      unless !versions
        return {
          :hack     => hack,
          :versions => versions,
        }.to_json
      end
    else
      return {
        error: "No such hack!",
      }.to_json
    end
  end

  def get_version
    version = Version.first("WHERE id=? AND hack_id=? AND is_deleted=false", [params[:version_id], params[:hack_id]])
    unless !version
      return version.to_json
    else
      return {
        error: "No such hack or version!",
      }.to_json
    end
  end

  def bulk
    response = {} of Int64 => NamedTuple(hack: Hack, versions: Array(Version))
    hacks = Hack.all("WHERE is_deleted=false")
    versions = Version.all("WHERE is_deleted=false")
    hacks.each do |hack|
      response[hack.id!] = {
        hack:     hack,
        versions: versions.select { |v| v.hack_id == hack.id! },
      }
    end
    return response.to_json
  end
end
