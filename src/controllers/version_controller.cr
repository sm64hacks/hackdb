class VersionController < ApplicationController
  def new
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      unless !hack
        render("new.ecr")
      end
    end
  end

  def create
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      unless !hack
        unless hack.id == nil
          hack_id = hack.id
        else
          hack_id = params[:id]
        end
        Version.create(name: params[:name], comment: params[:comment], contributors: params[:contributors], release_date: params[:release_date], download_url: params[:download_url], everdrive: "", hack_id: hack_id, user_id: session[:user_id], is_deleted: false)
        flash[:info] = "Version added to the database!"
        redirect_to "/hacks"
      end
    end
  end

  def edit
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      version = Version.first("WHERE is_deleted=false AND id=?", [params[:vid]])
      unless !hack
        unless !version
          render("edit.ecr")
        end
      end
    end
  end

  def update
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      version = Version.first("WHERE is_deleted=false AND id=?", [params[:vid]])
      unless !hack
        unless !version
          version.name = params[:name]
          version.comment = params[:comment]
          version.contributors = params[:contributors]
          version.release_date = params[:release_date]
          version.download_url = params[:download_url]
          version.save
          flash[:info] = "Version updated!"
          redirect_to "/hack/#{params[:id]}"
        end
      end
    end
  end

  def delete
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      version = Version.first("WHERE is_deleted=false AND id=?", [params[:vid]])
      unless !hack
        unless !version
          render("delete.ecr")
        end
      end
    end
  end

  def confirmdelete
    user = User.first("WHERE id = ?", [session[:user_id]])
    unless !user
      hack = Hack.first("WHERE is_deleted=false AND id=?", [params[:id]])
      version = Version.first("WHERE is_deleted=false AND id=?", [params[:vid]])
      unless !hack
        unless !version
          version.is_deleted = true
          version.save
          flash[:danger] = "Version deleted!"
          redirect_to "/hack/#{params[:id]}"
        end
      end
    end
  end
end
