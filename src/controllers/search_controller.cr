class SearchController < ApplicationController
  def index
    render("index.ecr")
  end

  def results
    query = params[:query] + "%"
    results = Hack.all("WHERE name LIKE ?", [query])
    render("results.ecr")
  end
end
