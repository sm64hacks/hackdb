class HTTP::Server::Context
  property current_user : User?
end

class AdminAuthenticate < Amber::Pipe::Base
  def call(context)
    user_id = context.session["user_id"]?
    if user = User.find user_id
      if user.is_admin == true
        context.current_user = user
        call_next(context)
      else
        return call_next(context) if public_path?(context.request.path)
        context.flash[:warning] = "Not Authorized!"
        context.response.headers.add "Location", "/"
        context.response.status_code = 302
      end
    else
      return call_next(context) if public_path?(context.request.path)
      context.flash[:warning] = "Please Sign In"
      context.response.headers.add "Location", "/signin"
      context.response.status_code = 302
    end
  end

  private def public_path?(path)
    return false
  end
end
