Amber::Server.configure do
  pipeline :web do
    # Plug is the method to use connect a pipe (middleware)
    # A plug accepts an instance of HTTP::Handler
    plug Amber::Pipe::ClientIp.new(["X-Forwarded-For"])
    plug Citrine::I18n::Handler.new
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
    plug Amber::Pipe::Flash.new
    plug Amber::Pipe::CSRF.new
    plug Authenticate.new
  end

  pipeline :admin do
    # Plug is the method to use connect a pipe (middleware)
    # A plug accepts an instance of HTTP::Handler
    plug Amber::Pipe::ClientIp.new(["X-Forwarded-For"])
    plug Citrine::I18n::Handler.new
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
    plug Amber::Pipe::Flash.new
    plug Amber::Pipe::CSRF.new
    plug AdminAuthenticate.new
  end

  pipeline :api do
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
    # plug Amber::Pipe::CORS.new
  end

  # All static content will run these transformations
  pipeline :static do
    plug Amber::Pipe::PoweredByAmber.new
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Static.new("./public")
  end

  routes :web do
    get "/hacks", HackController, :index
    get "/hack/:id", HackController, :show
    get "/author/:creator", HackController, :creator
    get "/hack/:id/version/:vid/flag", FlagController, :new
    post "/hack/:id/version/:vid/flag", FlagController, :create
    get "/search", SearchController, :index
    post "/search", SearchController, :results
    get "/profile", UserController, :show
    get "/profile/edit", UserController, :edit
    patch "/profile", UserController, :update
    get "/signin", SessionController, :new
    post "/session", SessionController, :create
    get "/signout", SessionController, :delete
    get "/signup", RegistrationController, :new
    post "/registration", RegistrationController, :create
    get "/api", HomeController, :api_doc
    get "/", HomeController, :index
  end

  routes :admin do
    get "/admin", AdminController, :index
    get "/admin/hack/new", HackController, :new
    post "/admin/hack/create", HackController, :create
    get "/admin/hack/:id/edit", HackController, :edit
    post "/admin/hack/:id/update", HackController, :update
    get "/admin/hack/:id/delete", HackController, :delete
    post "/admin/hack/:id/delete", HackController, :confirmdelete
    get "/admin/hack/:id/version/add", VersionController, :new
    post "/admin/hack/:id/version/create", VersionController, :create
    get "/admin/hack/:id/version/:vid/edit", VersionController, :edit
    post "/admin/hack/:id/version/:vid/update", VersionController, :update
    get "/admin/hack/:id/version/:vid/delete", VersionController, :delete
    post "/admin/hack/:id/version/:vid/delete", VersionController, :confirmdelete
  end

  routes :api do
    get "/api/hack/:id", ApiController, :get_hack
    get "/api/hack/:hack_id/:version_id", ApiController, :get_version
    get "/api/bulk", ApiController, :bulk
  end

  routes :static do
    # Each route is defined as follow
    # verb resource : String, controller : Symbol, action : Symbol
    get "/*", Amber::Controller::Static, :index
  end
end
