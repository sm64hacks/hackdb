-- +micrate Up
CREATE TABLE flags (
  id BIGSERIAL PRIMARY KEY,
  version_id BIGINT,
  reason TEXT,
  ipaddress TEXT,
  is_deleted BOOL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
CREATE INDEX flag_version_id_idx ON flags (version_id);

-- +micrate Down
DROP TABLE IF EXISTS flags;
