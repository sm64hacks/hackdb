-- +micrate Up
CREATE TABLE forums (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR,
  description TEXT,
  topic_count INT,
  post_count INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS forums;
