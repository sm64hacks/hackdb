-- +micrate Up
CREATE TABLE hacks (
  id BIGSERIAL PRIMARY KEY,
  name TEXT,
  creator TEXT,
  description TEXT,
  download_url TEXT,
  difficulty TEXT,
  total_stars TEXT,
  required_stars TEXT,
  youtube_url TEXT,
  fandom_url TEXT,
  is_deleted BOOL,
  user_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
CREATE INDEX hack_user_id_idx ON hacks (user_id);

-- +micrate Down
DROP TABLE IF EXISTS hacks;
