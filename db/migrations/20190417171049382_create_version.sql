-- +micrate Up
CREATE TABLE versions (
  id BIGSERIAL PRIMARY KEY,
  name TEXT,
  comment TEXT,
  contributors TEXT,
  everdrive TEXT,
  release_date TEXT,
  download_url TEXT,
  is_deleted BOOL,
  hack_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
CREATE INDEX version_hack_id_idx ON versions (hack_id);

-- +micrate Down
DROP TABLE IF EXISTS versions;
